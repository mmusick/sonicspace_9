# Sonic Space no. 9; #
### for deliberate sculptural agents ###

* This is a sonic ecosystem composition from the Sonic Spaces Project
* The code is intended to run SuperCollider on Raspberry Pi's

For more information about this work, please visit: [michaelmusick.com](http://michaelmusick.com/ss9/)