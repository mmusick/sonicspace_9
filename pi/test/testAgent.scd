/******************************************************************
      Author: Michael Musick
       Email: michael@michaelmusick.com


     Project:
        File: testAgent.scd

     Version: 1.0
      M&Year: Feb 2016

 Description: Test that the microphone and speaker are working with SC
       Notes:

		(~dir +/+ "/test/testAgent.scd").loadPaths;

******************************************************************/
(

"You should hear feedback?".postln;

{
	var env;
	env = Env.linen(1, 3, 1);
	env = EnvGen.kr(env, doneAction: 2);
	Out.ar(0,
		Limiter.ar(
			DelayC.ar(SoundIn.ar(0)*2, 0.1, 0.1) * env,
			level: 0.95
		);
	);
}.play;
);