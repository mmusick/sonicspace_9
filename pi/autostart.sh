#!/bin/bash

echo "Starting up Sonic Space 10, sclang, and Jackd"

cont=true
# Turn the following sleep back on if this is a boot time script
sleep 20

while $cont
	do
    	sudo killall jackd
		sudo killall sclang
		sleep 5

		sudo /usr/local/bin/jackd -P95 -dalsa -dhw:1 -p1024 -n3 -s -r 44100 &
		sleep 2
		su root -l -c "/usr/local/bin/sclang /home/pi/start.scd"
		echo $?
		
		sleep 5

		cont=true
    		echo "SuperCollider ended - try again"
done
